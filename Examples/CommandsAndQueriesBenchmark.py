'''
PyEOQ Example: CommandsAndQueriesBenchmark.py
----------------------------------------------------------------------------------------
This example gives a comprehensive list of commands and executes them serially in a 
benchmark. It gives the execution time of each command and query type. This can also be
used as a basic test if all commands work properly. 

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreSingleFileMdbProvider,PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2.action.externalpy import ExternalPyScriptHandler
from eoq2 import Get,Set,Add,Rem,Mov,Clo,Crt,Crn,Sts,Chg,Gaa,Cal,Asc,Abc,Cmp,CloModes
from eoq2 import Qry,Obj,His,Cls,Ino,Met,Idx,Pth,Arr
from eoq2.serialization import JsonSerializer,TextSerializer,PySerializer,JsSerializer

from eoq2.util import Backupper,NoLogging,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS

from timeit import default_timer as timer 
        
'''
MAIN: Execution starts here
'''          
if __name__ == '__main__':
#Basic configuration
    WORKSPACE_MDB = True
    WORKSPACE_DIR = 'Workspace'
    LOG_DIR = './log'
    MODELFILE = "MinimalFlightControl.oaam"

    #Create a model data base (MDB) (chose one of the following lines)
    mdbProvider = None
    if(WORKSPACE_MDB):
        mdbProvider = PyEcoreWorkspaceMdbProvider(WORKSPACE_DIR,metaDir=['./Meta'],saveTimeout=1.0)
    else:
        mdbProvider = PyEcoreSingleFileMdbProvider(WORKSPACE_DIR+"/"+MODELFILE,WORKSPACE_DIR+"/Meta/oaam.ecore",saveTimeout=1.0)
        

    #Create an encoding strategy for model based data 
    valueCodec = PyEcoreIdCodec()

    #Create an unique accessor to the data
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)

    #Create a domain and couple it with the mdb provider
    domain = LocalMdbDomain(mdbAccessor)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    #Register external actions (optional)
    #externalActionHandler = ExternalPyScriptHandler(domain.cmdRunner.callManager,'../Examples/workspace/actions',logger=logger)

    #prepare serializer
    serializer = TextSerializer()
    
    #Retrieve the model resource for the file we would like to work on. (Single file and workspace mdb have different solutions)
    root = None
    if(WORKSPACE_MDB):
        cmd = Get(Pth('resources').Sel(Pth('name').Equ(MODELFILE)).Idx(0).Pth('contents').Idx(0)) #workspace mdb
        jsonCmd = serializer.Ser(cmd) #workspace mdb
        print(jsonCmd) #workspace mdb
        root = domain.Do(cmd) #workspace mdb
    else:
        root = mdbAccessor.GetRoot() #single file mdb 
    
    #Define a list of benchmark commands
    cmds = [
            #any
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('FLATTEN').Any(Obj(0))),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Any(Arr([Obj(14),Obj(21)]))),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Any(Arr([Obj(14),Obj(21)])).Not()),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Sel(  Pth('tasks').Any(Obj(14))  )),
              
            #all
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').All(Arr([Obj(14),Obj(17)]))),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').All(Arr([Obj(14),Obj(17)])).Not()),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions')),
    
            #test instance of 
            Get(Qry(root).Ino('EObject')),
            #test add
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('tasks')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks')),
            Add(Qry(root).Pth('functions').Pth('subfunctions').Idx(0),'tasks',Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks').Arr([Idx(0),Idx(1)])),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('tasks')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks')),
            Add(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0),'tasks',Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('tasks').Arr([Idx(0),Idx(1)])),
            #test remove
            Cmp().Crn('http://www.oaam.de/oaam/model/v140/functions','Task',1)
                 .Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0))
                 .Add(His(1),'tasks',His(0)) #add the new task
                 .Rem(His(1),'tasks',His(0)), #remove it again
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('tasks')),
            #test mov
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks')),
            Mov(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks').Idx(0),5),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks')),
            Mov(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks').Idx(5),0),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks')),
            #test clone
            Clo(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions'),CloModes.CLS),
            Clo(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions'),CloModes.ATT),
            Clo(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions'),CloModes.FLT),
            Clo(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions'),CloModes.DEP),
            Clo(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions'),CloModes.FUL),
            #test create
            Get(Qry(root).Cls('Task').Pth('name')),
            Get(Qry(root).Cls('Task').Sel(Pth('name').Equ('ElevatorR1'))),
            Crt(Qry(root).Cls('Task').Sel(Pth('name').Equ('ElevatorR1')).Idx(0).Met('CLASS'),5),
            #test create by name
            Crn('http://www.oaam.de/oaam/model/v140/functions','Task',6),
            #test set
            Cmp().Crn('http://www.oaam.de/oaam/model/v140/functions','Task',1)
                .Crn('http://www.oaam.de/oaam/model/v140/library','TaskType',1)
                .Set(His(0),['name','type'],Arr(['New Task',His(1)])),
            #test set
            Get(Qry(root).Cls('Task').Sel(Pth('name').Equ('ElevatorR1'))),
            Set(Qry(root).Cls('Task').Sel(Pth('name').Equ('ElevatorR1')),'name','ElevatorR5589'),
            Get(Qry(root).Cls('Task').Sel(Pth('name').Equ('ElevatorR1'))),
            Set(Qry(root).Cls('Task').Sel(Pth('name').Equ('ElevatorR5589')),'name','ElevatorR1'), #revert change
            #test select
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks').Idx(0).Pth('type').Pth('name')),
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Equ('Elevator').Mul(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            Get(Qry(root).Cls('Task').Csp(Qry(root).Cls('Signal')).Sel(Idx(1).Pth('source').Met('CONTAINER').Equ(Idx(0)))),
            #test meta
            Get(Met('METAMODELS')),
            Get(Qry(root).Met('CLASS')),
            Get(Met('CLASS',['http://www.oaam.de/oaam/model/v140/functions','Task'])), #context less
            Get(Qry(root).Met('CLASSNAME')),
            Get(Qry(root).Met('CONTAINER')),
            Get(Qry(root).Met('PARENT')),
            Get(Qry(root).Cls('Task').Idx(0).Met('ALLPARENTS')),
            Get(Qry(root).Cls('Task').Idx(0).Met('ASSOCIATES').Met('CLASSNAME')),
            Get(Qry(root).Cls('Task').Idx(0).Met('ASSOCIATES',[Qry(root).Pth('allocations')]).Met('CLASSNAME')),
            Get(Qry(root).Met('INDEX')),
            Get(Qry(root).Met('CONTAININGFEATURE')),
            Get(Qry(root).Met('FEATURES')),
            Get(Qry(root).Met('FEATURENAMES')),
            Get(Qry(root).Met('FEATUREVALUES')),
            Get(Qry(root).Met('ATTRIBUTES')),
            Get(Qry(root).Met('ATTRIBUTENAMES')),
            Get(Qry(root).Met('ATTRIBUTEVALUES')),
            Get(Qry(root).Met('REFERENCES')),
            Get(Qry(root).Met('REFERENCENAMES')),
            Get(Qry(root).Met('REFERENCEVALUES')),
            Get(Qry(root).Met('CONTAINMENTS')),
            Get(Qry(root).Met('CONTAINMENTNAMES')),
            Get(Qry(root).Met('CONTAINMENTVALUES')),
            Get(Qry(root).Met('IF',[Met("PARENT"),Met("PARENT").Pth("name"),"no"])),
            Get(Met('IF',[Met("PARENT"),Met("PARENT").Pth("name"),"no"])),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('FLATTEN')),
            #test class meta
            Get(Qry(root).Cls('Task').Idx(0).Met('CLASS').Met('PACKAGE').Pth('name')),
            Get(Qry(root).Cls('Task').Idx(0).Met('CLASS').Met('SUPERTYPES').Pth('name')),
            Get(Qry(root).Cls('Task').Idx(0).Met('CLASS').Met('ALLSUPERTYPES').Pth('name')),
            Get(Qry(root).Cls('Task').Idx(0).Met('CLASS').Met('IMPLEMENTERS').Pth('name')),
            Get(Qry(root).Cls('Task').Idx(0).Met('CLASS').Met('ALLIMPLEMENTERS').Pth('name')),
             
            #index
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx(0)),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('FLATTEN')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('SIZE')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('SORTASC')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('SORTDSC')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx([0,6,2])), #extract range
             
            #test neq
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Neq('Elevator').Mul(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            #test gre
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator').Mul(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            #test les
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Les('Elevator').Mul(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            #test add
            Get(Qry(root).Cls('Task').Pth('outputs').Met('SIZE').Add(2)),
            #test sub
            Get(Qry(root).Cls('Task').Pth('outputs').Met('SIZE').Sub(2)),
            #test mul
            Get(Qry(root).Cls('Task').Pth('outputs').Met('SIZE').Mul(2)),
            #test dev
            Get(Qry(root).Cls('Task').Pth('outputs').Met('SIZE').Div(2)),
            #test csp
            Get(Qry(root).Cls('Task').Csp(Qry(root).Cls('Signal'))),
            #test its
            Get(Qry(root).Cls('Task').Sel(Pth('name').Les('ElevatorR1')).Its(Qry(root).Cls('Task').Sel(Pth('name').Gre('AileronR1'))).Pth('name')),
            #test dif
            Get(Qry(root).Cls('Task').Sel(Pth('name').Les('ElevatorR1')).Dif(Qry(root).Cls('Task').Sel(Pth('name').Gre('AileronR1'))).Pth('name')),
            #test uni
            Get(Qry(root).Cls('Task').Sel(Pth('name').Les('ElevatorR1')).Uni(Qry(root).Cls('Task').Sel(Pth('name').Gre('AileronR1'))).Pth('name')),    
            #test con
            Get(Qry(root).Cls('Task').Sel(Pth('name').Les('ElevatorR1')).Con(Qry(root).Cls('Task').Sel(Pth('name').Gre('AileronR1'))).Pth('name')),
            #test change
            Chg(0,5),
            Chg(3,0),
            
            #logical connection of statements for select
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator')).Pth('name')),
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator').Mul(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator').Mul(Pth('outputs').Met('SIZE').Equ(1).Mul(Pth('name').Les('XXXXX')))).Pth('name')),
 
            #empty list operations
            Get(Qry(root).Cls('Task').Sel(Pth('name').Equ('XXXX'))), # --> []
            Get(Qry(root).Cls('Task').Sel(Pth('name').Equ('XXXX')).Met('CLASS')),# --> []
            Get(Qry(root).Cls('Task').Sel(Pth('name').Equ('XXXX')).Sel(Pth('name').Equ('YYYY')).Met('CLASS')), # --> []

            #terminate statement
            Get(Qry(root).Cls('ConnectionAssignmentSegment').Pth('location').Trm().Pth('name')), #no fail
            Get(Qry(root).Cls('ConnectionAssignmentSegment').Pth('location').Trm().Met('CLASS').Met('PACKAGE').Pth('nsURI')), #no fail
            Get(Qry(root).Cls('ConnectionAssignmentSegment').Pth('location').Trm().Met('CLASS').Met('PACKAGE').Uni([])),
            Get(Qry(root).Cls('SignalAssignmentSegment').Trm(Qry().Met('SIZE').Equ(0),2).Met('SIZE')), #custom terminate condition and return value
            Get(Qry(root).Cls('SignalAssignmentSegment').Trm(Qry().Met('SIZE').Equ(0),2).Met('SIZE').Add(3)),
            Get(Qry(root).Cls('ConnectionAssignmentSegment').Trm(Qry().Met('SIZE').Equ(0),2).Met('SIZE')), #custom terminate condition and return value
            Get(Qry(root).Cls('ConnectionAssignmentSegment').Trm(Qry().Met('SIZE').Equ(0),2).Met('SIZE').Add(3)),
            
            #zip operation
            Get(Qry(root).Cls('Task').Zip([Pth('name'),Pth('type'),Pth('type').Pth('name')])), 

            #boolean operations
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator').Orr(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator').Xor(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator').And(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator').Nad(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
           ]
    
    
    #Run an commands sequentially and print their output
    i = 1
    durs = []
    
    for cmd in cmds:
        start = timer()
        status = 'UNKNOWN'
        try:
            val = domain.Do(cmd)
            status = 'OK'
        except Exception as e:
            status = 'NOK'
        
        end = timer()
        dur = end-start
        cmdStr = serializer.Ser(cmd)
        print("%d:\t%s\t%.4f s\t%s"%(i,status,dur,cmdStr))
        
        durs.append(dur)
        
        i += 1
        
    #print total
    print("")
    print("n:   %d"%(len(durs)))
    print("Tot: %.4f s"%(sum(durs)))
    print("Min: %.4f s"%(min(durs)))
    print("Avg: %.4f s"%(sum(durs)/len(durs))) 
    print("Max: %.4f s"%(max(durs)))
