'''
PyEOQ Example: BasicWorkspaceMdb.py
----------------------------------------------------------------------------------------
This example shows how to initialize a local workspace MDB, link it to a domain, and use
the domain to issue queries. 

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2 import Get,Set,Add,Crn,Cmp,Rem #import commands
from eoq2 import Qry,Pth,Obj,His,Idx #import query segments
      
'''
MAIN: Execution starts here
'''            
if __name__ == '__main__':
    warehouseDb = 'GeneralStore.warehouse'
    articles = [
        ("food/fresh/fruit", "Apple", 0.25, 400),
        ("food/fresh/fruit", "Banana", 0.35, 1),
        ("food/fresh/fruit", "Orange", 0.45, 20),
        ("food/fresh/fruit", "Melon", 1.1, 2),
        ("food/fresh/vegetables", "Cucumber", 0.5, 4),
        ("food/fresh/vegetables", "Lettuce", 0.8, 2),
        ("food/fresh/milk/cheese", "Goat Cheese", 5.0, 0),
        ("food/fresh/milk/cheese", "Gouda", 2.0, 100),
        ("food/fresh/milk", "Cream", 0.5, 300),
        ("food/fresh/milk", "Milk", 0.6, 700),
        ("food/drinks", "Coffee", 2.0, 100),
        ("food/drinks", "Hot Chocolate", 2.5, 1),
        ("food/drinks/lemonade", "Coke", 1.0, 1000),
        ("food/drinks/lemonade", "Energy Drink", 10.0, 0),
    ]
    
    # Initialize an WorkspaceMDB
    mdbProvider = PyEcoreWorkspaceMdbProvider('Workspace',metaDir=['./Meta'],saveTimeout=0.5) 
    valueCodec = PyEcoreIdCodec() #create the default value codec that converts objects to #IDs
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    domain = LocalMdbDomain(mdbAccessor)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    # Look if the warehouse DB already exists and it in that case
    resource = domain.Do( Get(Pth('resources').Sel(Pth('name').Equ(warehouseDb)).Trm(Idx('SIZE').Les(1),None).Idx(0)) )
    if(resource):
        # Empty the content
        domain.Do( Rem(resource,'contents',Qry(resource).Pth('contents')) )
    else:
        # Create file 
        cmd = Cmp().Crn('http://www.eoq.de/workspacemdbmodel/v1.0','ModelResource',1).Set(His(0),'name',warehouseDb).Add(Obj(0),'resources',His(0)) #add the new resource
        res = domain.Do(cmd)
        resource = res[0]
    # Create the warehouse root
    cmd = Cmp().Crn('http://examples.eoq.de/warehouse','Warehouse',1).Add(Qry(resource),'contents',His(0)) #add the new resource
    res = domain.Do(cmd)
    warehouse = res[0]
    
    # Fill the warehouse with all the articles
    for a in articles:
        # Find the appropriate parent category
        parent = warehouse
        categoryName = a[0].split("/")
        for c in categoryName:
            category = domain.Do( Get(Qry(parent).Pth('categories').Sel(Pth('name').Equ(c)).Trm(Idx('SIZE').Les(1),None).Idx(0)) )
            if(not category):
                category = domain.Do(Crn('http://examples.eoq.de/warehouse','Category',1))
                domain.Do(Set(category,'name',c))
                domain.Do( Add(parent,'categories',category) )
            parent = category
        # Create article and set elements
        article = domain.Do(Crn('http://examples.eoq.de/warehouse','Article',1))
        domain.Do( Set(article,['name','price','sells'],[a[1],a[2],a[3]]) )
        domain.Do( Add(parent,'articles',article) )
    
    
