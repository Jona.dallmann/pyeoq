'''
PyEOQ Example: CommandsAndQueries.py
----------------------------------------------------------------------------------------
This example gives a comprehensive list of examples for nearly all command and query types. 
For each example execution times are measured to give and impression on the runtime of 
different command and query types. Moreover it shows the usage and output of most serializers.
In addition, different utility functions as logging, backup, and event handling are shown. 

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreSingleFileMdbProvider,PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2.action.externalpy import ExternalPyScriptHandler
from eoq2 import Get,Set,Add,Rem,Mov,Clo,Crt,Crn,Sts,Chg,Gaa,Cal,Asc,Abc,Cmp,CloModes,Hel,Ses,Gby,Obs,Ubs
from eoq2 import Qry,Obj,His,Cls,Ino,Met,Idx,Pth,Arr,Equ,Eqa,Neq,Les,Gre
from eoq2.serialization import JsonSerializer,TextSerializer,PySerializer,JsSerializer
from eoq2.event import EvtTypes

from eoq2.util import Backupper,NoLogging,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS

from timeit import default_timer as timer #used to time the command's execution time.

'''
OnEvent (EventCallback):
This function is registered below as an event handler for EOQ transactions. Transactions cause 
events when modifying model elements. All event types are handled by this basic event handler and 
printed to the screen according to the event type. By default not all events will be caught, but only 
those after the event observe command OBS and the unobserve command UBS. See the list of commands
for a better understanding.
'''
def OnEvent(evts,source):
    for evt in evts:
        if(evt.evt == EvtTypes.CST):
            print("EVT(%s): Status of call %d changed to %s"%(evt.k,evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.OUP):
            print("EVT(%s): Output of call %d on channel %s: %s"%(evt.k,evt.a[0],evt.a[1],evt.a[2]))
        elif(evt.evt == EvtTypes.CVA):
            print("EVT(%s): Result of call %d: %s"%(evt.k,evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.CHG):
            print("EVT(%s): Change (%d): %s t: %s, f: %s, n: %s, [was: v:%s, o: %s, f:%s, i:%s] (tid:%d)"%(evt.k,evt.a[0],evt.a[1],evt.a[2],evt.a[3],evt.a[4],evt.a[5],evt.a[6],evt.a[7],evt.a[8],evt.a[9]))
        elif(evt.evt == EvtTypes.MSG):
            print("EVT(%s): Message: %s"%(evt.k,evt.a))
        
'''
MAIN: Execution starts here
'''          
if __name__ == '__main__':
    #Basic configuration
    WORKSPACE_MDB = False
    WORKSPACE_DIR = 'Workspace'
    LOG_DIR = './log'
    MODELFILE = "MinimalFlightControl.oaam"

    #create a backup. This is only for testing purposes and not necessary (both lines needed)
    #backupper = Backupper([WORKSPACE_DIR,LOG_DIR])
    #backupper.CreateBackup()

    #define loglevels (chose one of the following lines)
    #logLevels = DEFAULT_LOG_LEVELS
    logLevels = DEFAULT_LOG_LEVELS+["change","transaction","event"]
    #logLevels = DEFAULT_LOG_LEVELS+[LogLevels.DEBUG,"change","transaction","event"]

    #initialize logger. For the file based logger this must happen after the backup. (chose one of the following lines)
    #logger = NoLogging() #no output at all
    #logger = ConsoleLogger() #only console output
    logger = ConsoleAndFileLogger(logDir=LOG_DIR,activeLevels=logLevels) #console and file output

    #Create a model data base (MDB) (chose one of the following lines)
    mdbProvider = None
    if(WORKSPACE_MDB):
        mdbProvider = PyEcoreWorkspaceMdbProvider(WORKSPACE_DIR,metaDir=['./Meta'],saveTimeout=1.0,logger=logger)
    else:
        mdbProvider = PyEcoreSingleFileMdbProvider(WORKSPACE_DIR+"/"+MODELFILE,WORKSPACE_DIR+"/Meta/oaam.ecore",saveTimeout=1.0,logger=logger)
        

    #Create an encoding strategy for model based data 
    valueCodec = PyEcoreIdCodec()

    #Create an unique accessor to the data
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)

    #Create a domain and couple it with the mdb provider
    domain = LocalMdbDomain(mdbAccessor,logger=logger)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    #Initialize a new session
    print("Opening session")
    sessionId = domain.Do(Hel('user','pw'))
    
    #Initialize the event listener (specific events must be enabled afterwards).
    print('My session ID: %s'%(sessionId))
    domain.Observe(OnEvent,sessionId=sessionId)
    
    #Register external actions (optional)
    #externalActionHandler = ExternalPyScriptHandler(domain.cmdRunner.callManager,'../Examples/workspace/actions',logger=logger)

    #prepare serializers
    serializer = JsonSerializer()
    serializer2 = TextSerializer()
    serializer3 = PySerializer()
    serializer4 = JsSerializer()

    #Retrieve the model resource for the file we would like to work on. (Single file and workspace mdb have different solutions)
    root = None
    if(WORKSPACE_MDB):
        cmd = Get(Pth('resources').Sel(Pth('name').Equ(MODELFILE)).Idx(0).Pth('contents').Idx(0)) #workspace mdb
        jsonCmd = serializer.Ser(cmd) #workspace mdb
        print(jsonCmd) #workspace mdb
        root = domain.Do(cmd) #workspace mdb
    else:
        root = mdbAccessor.GetRoot() #single file mdb

    #comprehensive list of commands
    cmds = [
#             #PROBLEM SECTION (DISABLED)
#             #these are commands that caused trouble in the past, but work today. This To be extended in the future for testing.
#             Get(Qry().Arr([1.1,2,True,0,None,100,'test','a',[1,2,3]]).Its(Qry().Arr([1.1,True,None,'a',[1,2,3]]))),
#             Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Met('FEATUREVALUES')),
#             Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Met('FEATUREVALUES').Idx([10,15,1])),
#             Get(Qry().Arr([1.1,2,True,0,None,100,'test','a',[1,2,3]]).Its(Qry().Arr([1.1,True,None,'a',[1,2,3]]))),
#             Add(Cls('Connection'),'startingPoints',Cls('Io').Idx(0)), #Multi target add
#             Rem(Cls('Connection'),'startingPoints',Cls('Io').Idx(0)), #Multi target rem
#                 
#             Add(Cls('Connection').Idx(0),['startingPoints','endPoints'],Cls('Io').Idx(0)), #Multi feature add
#             Rem(Cls('Connection').Idx(0),['startingPoints','endPoints'],Cls('Io').Idx(0)), #Multi feature add
#   
#             Add(Cls('Connection').Arr([Idx(0),Idx(1)]),'startingPoints',Cls('Io').Arr( [Arr([Idx(0)]),2] )), #Multi feature add wrong 1
#             Add(Cls('Connection').Arr([Idx(0),Idx(1)]),'startingPoints',Cls('Io').Arr( [Arr([Idx(0)]),Arr([Idx(0),Idx(2)]),2] )), #Multi feature add wrong 1
#             Add(Cls('Connection').Arr([Idx(0),Idx(1)]),'startingPoints',Cls('Io').Arr( [Arr([Idx(0)]),Arr([Idx(0),Idx(2)])] )), #Multi feature add
#             Rem(Cls('Connection').Arr([Idx(0),Idx(1)]),'startingPoints',Cls('Io').Arr( [Arr([Idx(0)]),Arr([Idx(0),Idx(2)])] )), #Multi feature add
#   
#             Add(Cls('Connection').Idx(0),['startingPoints','endPoints'],Cls('Io').Arr( [Arr([Idx(0)]),Arr([Idx(0),Idx(2)])] )), #Multi feature add
#             Rem(Cls('Connection').Idx(0),['startingPoints','endPoints'],Cls('Io').Arr( [Arr([Idx(0)]),Arr([Idx(0),Idx(2)])] )), #Multi feature add
#   
#             Add(Cls('Connection').Arr([Idx(0),Idx(1),Idx(2)]),['startingPoints','endPoints'],Cls('Io').Arr( [Arr([ Arr([]), Arr([Idx(2)]) ]),Arr([ Arr([Idx(2)]), Arr([Idx(2)]) ]),Arr([ Arr([]), Arr([]) ])] )), #Multi target, multi feature, multi value add
#             Rem(Cls('Connection').Arr([Idx(0),Idx(1),Idx(2)]),['startingPoints','endPoints'],Cls('Io').Arr( [Arr([ Arr([]), Arr([Idx(2)]) ]),Arr([ Arr([Idx(2)]), Arr([Idx(2)]) ]),Arr([ Arr([]), Arr([]) ])] )), #Multi target, multi feature, multi value rem
#      
#             Cmp().Get(Met('CLASS',['http://www.oaam.de/oaam/model/v140/common','OaamBaseElementA']))
#                  .Get(His(0).Met('IMPLEMENTERS').Pth('name'))
#                  .Get(His(0).Met('ALLIMPLEMENTERS').Pth('name')),
#             Cmp().Crn('http://www.oaam.de/oaam/model/v140/functions', 'Task', 1)
#                  .Set(His(-1), 'name', 'newTask')
#                  .Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0))
#                  .Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0))
#                  .Add(Arr([His(-2), His(-1)]), 'tasks', His(0) )
#                  .Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('tasks'))
#                  .Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('tasks')),
#             Get(Qry(root).Cls('TaskAssignment').Pth('task').Uni([])),
#             Get(Qry(root).Cls('Task').Dif(Cls('TaskAssignment').Pth('task'))),
#             Get(Qry(root).Cls('Task').Its(Cls('TaskAssignment').Pth('task'))),
#             Get(Qry(root).Cls('Task').Sel( Met('ASSOCIATES').Sel( Met('CLASS').Pth('name').Equ('TaskAssignment') ).Idx('SIZE').Gre(0) )),
#             
            #Get(Qry(root).Cls('Task').Sel(Met('ASSOCIATES').Cls('TaskAssignment').Idx('SIZE')))
#             Cmp().Get(Qry(root).Cls('Task').Idx(0))
#                  .Get(His(-1)),
#             Cmp().Crn('http://www.oaam.de/oaam/model/v140/hardware', 'Connection', 1)
#             .Set(His(-1), 'name', 'TestName')
#             .Get(His(-1)),
#             Cmp().Crn('http://www.oaam.de/oaam/model/v140/functions','Task',6)
#             .Get(His(-1).Pth('fixedRate')),
#             Get(Cls('Task').Pth('fixedRate')),
#             Get(His(-1)),
#             Get(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0).Pth('connections').Idx([3, 5, 1])),
#             Set(Qry(root).Pth('hardware').Pth('subhardware').Idx(0).Pth('subhardware').Idx(0).Pth('connections').Idx([3, 5, 1]), ['name', 'id'], [['testName1', 'testID1'],['testName2', 'testID2']]),
#             Get(Arr([1,2,3]).Zip([Arr([1,2,3]),Arr([1,2,3])])),
#             Get(Qry(root).Cls('Task').Sel(  Pth('name').Equ('BLABLA')  ).Sel( Pth('name').Neq('lkdsjfl')  )),
#             Get(Met('METAMODELS').Sel(Pth('nsURI').Equ('http://www.oaam.de/oaam/model/v140/functions')).Idx(0).Pth('eClassifiers').Sel(Pth('name').Equ('Task')).Idx(0)),
#             Get(Qry(root).Cls('Task')),
#             Get(Qry(root).Cls(Obj(21))),
#             Get(Met('METAMODELS').Sel(Pth('nsURI').Equ('http://www.eclipse.org/emf/2002/Ecore')).Idx(0).Pth('eClassifiers').Sel(Pth('name').Equ('EObject')).Idx(0)),
#             Get(Qry(root).Ino('EObject')),
#             Get(Qry(root).Ino(Obj(71))),
#             Get(Qry(root).Cls('Task').Idx(0)),
#             Get(Qry(root).Cls('Task').Idx(0).Met('ASSOCIATES',[Qry(root)])),
#             Get(Qry(root).Cls('Task').Idx(0).Met('ASSOCIATES',[Qry(root)])),
#             Get(Ino('EObject').Idx('SIZE')),
#       
            #REGULAR COMMANDS (ENABLED)
            #observe all events
            Cmp().Ses(sessionId) #session id must be known in order to observe events
                 .Obs('CHG','*'), #all change events
            #any
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('FLATTEN').Any(Obj(0))),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Any(Arr([Obj(14),Obj(21)]))),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Any(Arr([Obj(14),Obj(21)])).Not()),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Sel(  Pth('tasks').Any(Obj(14))  )),
                               
            #all
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').All(Arr([Obj(14),Obj(17)]))),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').All(Arr([Obj(14),Obj(17)])).Not()),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions')),
                     
            #test instance of 
            Get(Qry(root).Ino('EObject')),
            #test add
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('tasks')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks')),
            Add(Qry(root).Pth('functions').Pth('subfunctions').Idx(0),'tasks',Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks').Arr([Idx(0),Idx(1)])),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('tasks')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks')),
            Add(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0),'tasks',Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('tasks').Arr([Idx(0),Idx(1)])),
            
            #stop observing events.
            Cmp().Ses(sessionId) #session id must be known in order to observe events
                 .Ubs('CHG','*'), #all change events
                 
            #test remove
            Cmp().Crn('http://www.oaam.de/oaam/model/v140/functions','Task',1)
                 .Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0))
                 .Add(His(1),'tasks',His(0)) #add the new task
                 .Rem(His(1),'tasks',His(0)), #remove it again
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('tasks')),
            #test mov
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks')),
            Mov(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks').Idx(0),5),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks')),
            Mov(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks').Idx(5),0),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks')),
            #test clone
            Clo(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions'),CloModes.CLS),
            Clo(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions'),CloModes.ATT),
            Clo(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions'),CloModes.FLT),
            Clo(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions'),CloModes.DEP),
            Clo(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions'),CloModes.FUL),
            #test create
            Get(Qry(root).Cls('Task').Pth('name')),
            Get(Qry(root).Cls('Task').Sel(Pth('name').Equ('ElevatorR1'))),
            Crt(Qry(root).Cls('Task').Sel(Pth('name').Equ('ElevatorR1')).Idx(0).Met('CLASS'),5),
            #test create by name
            Crn('http://www.oaam.de/oaam/model/v140/functions','Task',6),
            #test set
            Cmp().Crn('http://www.oaam.de/oaam/model/v140/functions','Task',1)
                .Crn('http://www.oaam.de/oaam/model/v140/library','TaskType',1)
                .Set(His(0),['name','type'],Arr(['New Task',His(1)])),
            #test rollback
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('tasks')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks')),
            Cmp().Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0))
                .Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks'))
                .Add(His(0),'tasks',His(1).Arr([Idx(3),Idx(1)]))
                .Rem(His(0),'tasks',His(0).Pth('tasks'))
                .Add(His(0).Pth('tasks'),'tasks',His(1).Arr([Idx(0),Idx(1)])),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('tasks')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks')),
            #test set
            Get(Qry(root).Cls('Task').Sel(Pth('name').Equ('ElevatorR1'))),
            Set(Qry(root).Cls('Task').Sel(Pth('name').Equ('ElevatorR1')),'name','ElevatorR5589'),
            Get(Qry(root).Cls('Task').Sel(Pth('name').Equ('ElevatorR1'))),
            Set(Qry(root).Cls('Task').Sel(Pth('name').Equ('ElevatorR5589')),'name','ElevatorR1'), #revert change
            #test select
            Get(Qry(root).Pth('functions').Pth('subfunctions').Idx(0).Pth('subfunctions').Idx(0).Pth('tasks').Idx(0).Pth('type').Pth('name')),
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Equ('Elevator').Mul(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            Get(Qry(root).Cls('Task').Csp(Qry(root).Cls('Signal')).Sel(Idx(1).Pth('source').Met('CONTAINER').Equ(Idx(0)))),
            #test meta
            Get(Met('METAMODELS')),
            Get(Qry(root).Met('CLASS')),
            Get(Met('CLASS',['http://www.oaam.de/oaam/model/v140/functions','Task'])), #context less
            Get(Qry(root).Met('CLASSNAME')),
            Get(Qry(root).Met('CONTAINER')),
            Get(Qry(root).Met('PARENT')),
            Get(Qry(root).Cls('Task').Idx(0).Met('ALLPARENTS')),
            Get(Qry(root).Cls('Task').Idx(0).Met('ASSOCIATES').Met('CLASSNAME')),
            Get(Qry(root).Cls('Task').Idx(0).Met('ASSOCIATES',[Qry(root).Pth('allocations')]).Met('CLASSNAME')),
            Get(Qry(root).Met('INDEX')),
            Get(Qry(root).Met('CONTAININGFEATURE')),
            Get(Qry(root).Met('FEATURES')),
            Get(Qry(root).Met('FEATURENAMES')),
            Get(Qry(root).Met('FEATUREVALUES')),
            Get(Qry(root).Met('ATTRIBUTES')),
            Get(Qry(root).Met('ATTRIBUTENAMES')),
            Get(Qry(root).Met('ATTRIBUTEVALUES')),
            Get(Qry(root).Met('REFERENCES')),
            Get(Qry(root).Met('REFERENCENAMES')),
            Get(Qry(root).Met('REFERENCEVALUES')),
            Get(Qry(root).Met('CONTAINMENTS')),
            Get(Qry(root).Met('CONTAINMENTNAMES')),
            Get(Qry(root).Met('CONTAINMENTVALUES')),
            Get(Qry(root).Met('IF',[Met("PARENT"),Met("PARENT").Pth("name"),"no"])),
            Get(Met('IF',[Met("PARENT"),Met("PARENT").Pth("name"),"no"])),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('FLATTEN')),
            #test class meta
            Get(Qry(root).Cls('Task').Idx(0).Met('CLASS').Met('PACKAGE').Pth('name')),
            Get(Qry(root).Cls('Task').Idx(0).Met('CLASS').Met('SUPERTYPES').Pth('name')),
            Get(Qry(root).Cls('Task').Idx(0).Met('CLASS').Met('ALLSUPERTYPES').Pth('name')),
            Get(Qry(root).Cls('Task').Idx(0).Met('CLASS').Met('IMPLEMENTERS').Pth('name')),
            Get(Qry(root).Cls('Task').Idx(0).Met('CLASS').Met('ALLIMPLEMENTERS').Pth('name')),
                              
            #index
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx(0)),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('FLATTEN')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('SIZE')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('SORTASC')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx('SORTDSC')),
            Get(Qry(root).Pth('functions').Pth('subfunctions').Pth('subfunctions').Pth('subfunctions').Pth('tasks').Idx([0,6,2])), #extract range
                          
            #test eqa
            Get( Qry(root).Cls('Task').Pth('name') ),
            Get( Qry(root).Cls('Task').Pth('name').Eqa(Arr(['ElevatorR1','AileronL1'])) ),
            Get( Qry(root).Cls('Task').Pth('name').Sel(Eqa(Arr(['ElevatorR1','AileronL1']))) ),
            Get( Qry(root).Cls('Task').Sel(Pth('name').Eqa(Arr(['ElevatorR1','AileronL1']))) ),    
                                      
            #test neq
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Neq('Elevator').Mul(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            #test gre
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator').Mul(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            #test les
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Les('Elevator').Mul(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            #test add
            Get(Qry(root).Cls('Task').Pth('outputs').Met('SIZE').Add(2)),
            #test sub
            Get(Qry(root).Cls('Task').Pth('outputs').Met('SIZE').Sub(2)),
            #test mul
            Get(Qry(root).Cls('Task').Pth('outputs').Met('SIZE').Mul(2)),
            #test dev
            Get(Qry(root).Cls('Task').Pth('outputs').Met('SIZE').Div(2)),
            #test csp
            Get(Qry(root).Cls('Task').Csp(Qry(root).Cls('Signal'))),
            #test its
            Get(Qry(root).Cls('Task').Sel(Pth('name').Les('ElevatorR1'))),
            Get(Qry(root).Cls('Task').Sel(Pth('name').Gre('AileronR1'))),
            Get(Qry(root).Cls('Task').Sel(Pth('name').Les('ElevatorR1')).Its(Qry(root).Cls('Task').Sel(Pth('name').Gre('AileronR1')))),
            #test dif
            Get(Qry(root).Cls('Task').Sel(Pth('name').Les('ElevatorR1')).Dif(Qry(root).Cls('Task').Sel(Pth('name').Gre('AileronR1')))),
            #test uni
            Get(Qry(root).Cls('Task').Sel(Pth('name').Les('ElevatorR1')).Uni(Qry(root).Cls('Task').Sel(Pth('name').Gre('AileronR1')))),
            #test con
            Get(Qry(root).Cls('Task').Sel(Pth('name').Les('ElevatorR1')).Con(Qry(root).Cls('Task').Sel(Pth('name').Gre('AileronR1')))),
            #test change
            Chg(0,5),
            Chg(3,0),
             
            #logical connection of statements for select
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator')).Pth('name')),
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator').Mul(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator').Mul(Pth('outputs').Met('SIZE').Equ(1).Mul(Pth('name').Les('XXXXX')))).Pth('name')),
             
            #empty list operations
            Get(Qry(root).Cls('Task').Sel(Pth('name').Equ('XXXX'))), # --> []
            Get(Qry(root).Cls('Task').Sel(Pth('name').Equ('XXXX')).Met('CLASS')),# --> []
            Get(Qry(root).Cls('Task').Sel(Pth('name').Equ('XXXX')).Sel(Pth('name').Equ('YYYY')).Met('CLASS')), # --> []
            
            #terminate statement
            Get(Qry(root).Cls('ConnectionAssignmentSegment').Pth('location').Trm().Pth('name')), #no fail
            Get(Qry(root).Cls('ConnectionAssignmentSegment').Pth('location').Trm().Met('CLASS').Met('PACKAGE').Pth('nsURI')), #no fail
            Get(Qry(root).Cls('ConnectionAssignmentSegment').Pth('location').Trm().Met('CLASS').Met('PACKAGE').Uni([])),
            Get(Qry(root).Cls('SignalAssignmentSegment').Trm(Qry().Met('SIZE').Equ(0),2).Met('SIZE')), #custom terminate condition and return value
            Get(Qry(root).Cls('SignalAssignmentSegment').Trm(Qry().Met('SIZE').Equ(0),2).Met('SIZE').Add(3)),
            Get(Qry(root).Cls('ConnectionAssignmentSegment').Trm(Qry().Met('SIZE').Equ(0),2).Met('SIZE')), #custom terminate condition and return value
            Get(Qry(root).Cls('ConnectionAssignmentSegment').Trm(Qry().Met('SIZE').Equ(0),2).Met('SIZE').Add(3)),
            
            #zip operation
            Get(Arr([1,2,3]).Zip([Arr([1,2,3]),Arr([1,2,3])])),
            Get(Qry(root).Cls('Task').Zip([Pth('name'),Pth('type'),Pth('type').Pth('name')])), 
           
            #boolean operations
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator').Orr(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator').Xor(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator').And(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            Get(Qry(root).Cls('Task').Sel(Pth('type').Pth('name').Gre('Elevator').Nad(Pth('outputs').Met('SIZE').Equ(1))).Pth('name')),
            
            #finished
            Gby(sessionId)
            ]
    
    
    #Run an commands sequentially and print their output
    testNr = 1
    failedTxt2PyTests = []
    for cmd in cmds:
        start = timer()
        jsonCmd = serializer.Ser(cmd)
        #test the different serializers as well as text parsing
        print("%d: CMD (JSON): %s"%(testNr,jsonCmd))
        print("%d: CMD (TXT): %s"%(testNr,serializer2.Ser(cmd)))
        try:
            res = serializer2.Des(serializer2.Ser(cmd))
            try:
                print(f"{testNr}: CMD (TXT2PY): Success! {res.cmd} {res.a}")
            except:
                print(f"{testNr}: CMD (TXT2PY): Success! {res}")
        except:
            print("%d: CMD (TXT2PY): Failed!"%(testNr))
            failedTxt2PyTests.append(testNr)
        print("%d: CMD (PY): %s"%(testNr,serializer3.Ser(cmd)))
        print("%d: CMD (JS): %s"%(testNr,serializer4.Ser(cmd)))
        cmd2 = serializer.Des(jsonCmd)
        #execute the command on the domain
        try:
            val = domain.Do(cmd2)
            print("Result: %s"%(val))
        except Exception as e:
            print("Command failed: %s"%(str(e)))

        # print the execution time
        end = timer()
        print("Command time: %f s"%(end-start))
        print("") #newline

        testNr += 1

    # print statistics on text parsing
    print(f"Txt2Py failed on {len(failedTxt2PyTests)} tests, explicitly on {failedTxt2PyTests}")
