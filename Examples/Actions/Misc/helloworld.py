'''
    A simple action to test the action server. 
    Retrieves the target object, looks if it has 
    a name and than renames it.
'''


def helloworld(domain : 'Domain', times : 'Integer')->'String':
    for i in range(times):
        print('Hello world!')
    return 'I printed %d times Hello world! Look at the output.'%(times)
