'''
    An action to slow down the query execution 
'''

import time

def progress(domain : 'Domain', seconds : 'Real=0.5:The time until 100% are reached', steps : 'Int=10:The number of steps to be printed'):
    stepSize = 100/steps
    for i in range(steps):
        print('Status %d%%.'%(stepSize*(i+1)))
        time.sleep(seconds)
    return