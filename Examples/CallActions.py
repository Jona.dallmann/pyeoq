'''
PyEOQ Example: CallActions.py
----------------------------------------------------------------------------------------
This example shows how start, control and stop EOQ actions. It gives examples for 
synchronous and asynchronous actions calls. In addition it shows how to access the 
output and return values of the action.

See EOQ User Manual for more information: https://gitlab.com/eoq/doc

2020 Bjoern Annighoefer
'''

from eoq2.mdb.pyecore import PyEcoreWorkspaceMdbProvider,PyEcoreMdbAccessor,PyEcoreIdCodec
from eoq2.domain.local import LocalMdbDomain

from eoq2.action.externalpy import ExternalPyScriptHandler
from eoq2 import Get,Gaa,Cal,Asc,Abc,Cmp,Hel,Gby,Obs # import session and action commands
from eoq2 import His,Pth #import necessary queries
from eoq2.serialization import JsonSerializer,TextSerializer,PySerializer,JsSerializer
from eoq2.event import EvtTypes

from eoq2.util import NoLogging,ConsoleLogger,ConsoleAndFileLogger,LogLevels,DEFAULT_LOG_LEVELS

'''
OnEvent (EventCallback):
This function is registered below as an event handler for EOQ actions. Actions cause 
events by modifying model elements, changing their status (INIT, RUN, FIN). In addition,
asynchronous events cause OUP events when writing to the console. All events are cached
this basic event handler, and printed to the screen according to the event type.
'''
def OnEvent(evts,source):
    for evt in evts: #the event handler allways gets a list of events.
        if(evt.evt == EvtTypes.CST):
            print("EVT: Status of call %d changed to %s"%(evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.OUP):
            print("EVT: Output of call %d on channel %s: %s"%(evt.a[0],evt.a[1],evt.a[2]))
        elif(evt.evt == EvtTypes.CVA):
            print("EVT: Result of call %d: %s"%(evt.a[0],evt.a[1]))
        elif(evt.evt == EvtTypes.CHG):
            print("EVT: Change (%d): %s t: %s, f: %s, n: %s, [was: v:%s, o: %s, f:%s, i:%s] (tid:%d)"%(evt.a[0],evt.a[1],evt.a[2],evt.a[3],evt.a[4],evt.a[5],evt.a[6],evt.a[7],evt.a[8],evt.a[9]))
        elif(evt.evt == EvtTypes.MSG):
            print("EVT: Message: %s"%(evt.a))
        
'''
MAIN: Execution starts here
'''        
if __name__ == '__main__':
    #Basic configuration
    workspaceDir = 'Workspace'
    logDir = './log'
    modelfile = "MinimalFlightControl.oaam"
    
    #define loglevels (chose one of the following lines)
    #logLevels = DEFAULT_LOG_LEVELS
    #logLevels = DEFAULT_LOG_LEVELS+["change","transaction","event"]
    #logLevels = DEFAULT_LOG_LEVELS+[LogLevels.DEBUG,"change","transaction","event"]
    
    #initialize logger. For the file based logger this must happen after the backup. (chose one of the following lines)
    logger = NoLogging() #no output at all
    #logger = ConsoleLogger() #only console output
    #logger = ConsoleAndFileLogger(logDir=logDir,activeLevels=logLevels) #console and file output
          
    #Create a model data base (MDB) (chose one of the following lines)
    mdbProvider = PyEcoreWorkspaceMdbProvider(workspaceDir,metaDir=['./Meta'],saveTimeout=1.0,logger=logger)
    
    #Create an encoding strategy for model based data 
    valueCodec = PyEcoreIdCodec()
    
    #Create an unique accessor to the data
    mdbAccessor = PyEcoreMdbAccessor(mdbProvider.GetMdb(),valueCodec)
    
    #Create a domain and couple it with the mdb provider
    domain = LocalMdbDomain(mdbAccessor,logger=logger)
    mdbProvider.CoupleWithDomain(domain, valueCodec)
    
    #Initialize a new session
    print("Opening session")
    sessionId = domain.Do(Hel('user','pw'))
    print('My session ID: %s'%(sessionId))
    
    #listen to all events
    domain.Observe(OnEvent,sessionId=sessionId)
    domain.Do(Obs('*','*'),sessionId=sessionId)
    
    #Register external actions (see folder Examples/Actions/...)
    externalActionHandler = ExternalPyScriptHandler(domain.cmdRunner.callManager,'Actions',logger=logger)
    
    #Create serializers
    serializer = JsonSerializer()
    serializer2 = TextSerializer()
    serializer3 = PySerializer()
    serializer4 = JsSerializer()
    
    #Retrieve the model resource for the file we would like to work on. 
    cmd = Get(Pth('resources').Sel(Pth('name').Equ(modelfile)).Idx(0).Pth('contents').Idx(0)) 
    jsonCmd = serializer.Ser(cmd) 
    print(jsonCmd) 
    root = domain.Do(cmd) 
    
    
    #Define a list of commands to be executed
    cmds = [
            #test nested calls
            Asc('Misc/nestedcalls'),
            #test get all actions
#             Gaa(),
#             #test sync call
#             Cal('Misc/helloworld',[3]),
#             #test async call
#             Asc('Misc/progress',[1,10]),
#             Cal('Misc/sleep',[10]), #wait some time
#             #stop listening to events
#             Cmp().Ses(sessionId)
#                  .Ubs('*','*'),
#             #test abort call
#             Asc('Misc/progress',[1,10]),
#             Cal('Misc/sleep',[4]), #wait some time
#             Abc(3),
#             #test imidiate abort
#             Cmp().Asc('Misc/abortTest',[2])
#                  .Abc(His(0)),
#             #test sync action output is async
#             Cal('Misc/progress',[1,10]),
#             
#             #finished
#             Gby(sessionId)
           ]
    
    
    #Run an commands sequentially and print their output
    testNr = 1
    for cmd in cmds:
        #serialize commands in various ways and print them to the screen
        jsonCmd = serializer.Ser(cmd)
        print("%d: CMD (JSON): %s"%(testNr,jsonCmd))
        print("%d: CMD (TXT): %s"%(testNr,serializer2.Ser(cmd)))
        print("%d: CMD (PY): %s"%(testNr,serializer3.Ser(cmd)))
        print("%d: CMD (JS): %s"%(testNr,serializer4.Ser(cmd)))
        #test deserialization
        cmd2 = serializer.Des(jsonCmd)
        #Execute the a command by calling the domain
        try:
            val = domain.Do(cmd2)
            print("Result: %s"%(val))
        except Exception as e:
            print("Command failed: %s"%(str(e)))
        
        print(""); #newline
        testNr += 1
        