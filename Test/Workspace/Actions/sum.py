def sum(domain: 'Domain', arg1: 'Integer', arg2: 'Integer') -> 'returns sum':
    return arg1 + arg2
