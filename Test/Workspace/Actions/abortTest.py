import time

def abortTest(domain : 'Domain', seconds : 'Real=1.0:The sleep time in seconds'):
    time.sleep(seconds)
    return