This directory contains the files necessary files to generate internal models of the PyEOQ. 

The models files (*.ecore) contained in this folder are copies from the dedicated model repositories:
https://gitlab.com/eoq/models

If you want to to generate from the latest model make sure to copy the latest version of the model files 
from the dedicated model repositories.

Prerequisite for the generation is pyecoregen.